#include "Board.h"
#include "Bishop.h"
#include "King.h"
#include "Knight.h"
#include "Pawn.h"
#include "Rook.h"
#include "Queen.h"

 Board::Board()
{
	this->_soldiers.resize(BOARD_LEN);
	this->_blackPieces.resize(BOARD_LEN * 2);
	this->_whitePieces.resize(BOARD_LEN * 2);
	for (int i = 0; i < this->_soldiers.size(); i++) //initializing the board
	{
		this->_soldiers[i].resize(BOARD_LEN);
	}
	//setting pawns
	for (int i = 0; i < BOARD_LEN; i++) {
		
		this->_soldiers[6][i] = new Pawn('P', true, false, i, 6); //White pawns
		
		this->_soldiers[1][i] = new Pawn('p', false, false, i, 1);//Black pawns
	
	}
	//blacks Pieces
	this->_soldiers[0][0] = new Rook('r', false, false, 0, 0);
	this->_soldiers[0][7] = new Rook('r', false, false, 7, 0);
	this->_soldiers[0][2] = new Bishop('b', false, false, 2, 0);
	this->_soldiers[0][5] = new Bishop('b', false, false, 5, 0);
	this->_soldiers[0][1] = new Knight('n', false, false, 1, 0);
	this->_soldiers[0][6] = new Knight('n', false, false, 6, 0);
	this->_soldiers[0][3] = new Queen('q', false, false, 3, 0);
	this->_soldiers[0][4] = new King('k', false, false, 4, 0);
	//white pieces
	this->_soldiers[7][0] = new Rook('R', true, false, 0, 7);
	this->_soldiers[7][7] = new Rook('R', true, false, 7, 7);
	this->_soldiers[7][2] = new Bishop('B', true, false, 2, 7);
	this->_soldiers[7][5] = new Bishop('B', true, false, 5, 7);
	this->_soldiers[7][1] = new Knight('N', true, false, 1, 7);
	this->_soldiers[7][6] = new Knight('N', true, false, 6, 7);
	this->_soldiers[7][3] = new Queen('Q', true, false, 3, 7);
	this->_soldiers[7][4] = new King('K', true, false, 4, 7);
	//setting the blanks
	for (int i = 2; i < 6; i++)
	{
		for (int j = 0; j < BOARD_LEN; j++)
		{
			this->_soldiers[i][j] = new Pawn('#', false, true, j, i); //isEmpty = true


		}
	}
}


 vector<vector<Piece*>> Board::getBoard()
 {
	 return _soldiers;
 }


void Board::move(Spot fromSpot, Spot toSpot)
{
	this->_soldiers[toSpot.getY()][ toSpot.getX()] = this->_soldiers[fromSpot.getY()][ fromSpot.getX()]; 
	this->_soldiers[toSpot.getY()][toSpot.getX()]->_current = toSpot;
	this->_soldiers[fromSpot.getY()][fromSpot.getX()] = new Pawn('#', false, true, fromSpot.getX(), fromSpot.getY()); //
}

Piece* Board::getPieceInSpot(Spot curr)
{
	return this->_soldiers[curr.getY()][curr.getX()];
}


void Board::setPieceInSpot(Spot curr, Piece* next)
{
	this->_soldiers[curr.getY()][curr.getX()] = next;
}
