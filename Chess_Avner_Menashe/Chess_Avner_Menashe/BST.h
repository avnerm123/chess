#pragma once
#include <iostream>
#include "move.h"


class BST
{
	move data;
	BST *left, *right;

public:
	// Parameterized constructor. 
	BST(move value);
	BST();
	// Insert function. 
	bool hesRight();
	bool hesLeft();
	BST* getRight();
	BST* getLeft();
	move getData();

	BST* Insert(BST *, move);
	
	// Inorder traversal. 
	void Inorder(BST *);
};
