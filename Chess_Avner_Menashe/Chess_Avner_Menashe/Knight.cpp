#include "Knight.h"

Knight::Knight(char kind, bool isWhite, bool isEmpty, int x, int y) : Piece(kind, isWhite, isEmpty, x, y)
{
}

Knight::~Knight()
{
}


bool Knight::isValidMove(Board board, Spot next)
{
	if (!board.getPieceInSpot(next)->isEmpty())
	{
		if (this->isWhite() == board.getPieceInSpot(next)->isWhite())
		{
			return false;
		}
	}
	if (this->isEmpty())
	{
		return false;
	}
	this->getValidMoves(board);
	for (int i = 0; i < this->LegelMoves.size(); i++)
	{
		Spot curr = this->LegelMoves[i];
		if (curr.getX() == next.getX() && curr.getY() == next.getY())
		{
			return true;
		}
	}
	return false;
}



bool Knight::firstColum(int pieceTile, int offset) {
	int firstColumArr[] = { 0,8,16,24,32,40,43,56 };
	bool temp = false;

	for (int i : firstColumArr)
	{
		if (pieceTile == i)
		{
			temp = true;
			break;
		}
		else
			temp = false;

	}

	return temp && ((offset == -17) || (offset == -10) || (offset == 6) || (offset == 15));
}

bool Knight::secondColum(int pieceTile, int offset) {
	int secondColumArr[] = { 1,9,17,25,33,41,49,55 };
	bool temp = false;

	for (int i : secondColumArr)
	{
		if (pieceTile == i)
		{
			temp = true;
			break;
		}
		else
			temp = false;

	}

	return temp && ((offset == -10) || (offset == 6));
}

bool Knight::seventhColum(int pieceTile, int offset) {
	int seventhColumArr[] = { 6,14,22,30,38,46,54,62 };
	bool temp = false;

	for (int i : seventhColumArr)
	{
		if (pieceTile == i)
		{
			temp = true;
			break;
		}
		else
			temp = false;

	}

	return temp && ((offset == -6) || (offset == 10));
}

bool Knight::eighthColum(int pieceTile, int offset) {
	int eighthColumArr[] = { 7,15,23,31,39,47,55,63 };
	bool temp = false;

	for (int i : eighthColumArr)
	{
		if (pieceTile == i)
		{
			temp = true;
			break;
		}
		else
			temp = false;
	}

	return temp && ((offset == -15) || (offset == -6) || (offset == 10) || (offset == 17));
}

bool Knight::isValidTileCoordinate(int currentCandidate)
{
	return currentCandidate >= 0 && currentCandidate < 64;
}


void Knight::getValidMoves(Board board)
{
	this->LegelMoves.resize(0);
	int possibleDestinationTile;
	int offset;

	for (int i = 0; i < 8; i++)
	{
		offset = this->CANDIDATE_MOVE_COORDINATES[i];
		possibleDestinationTile = this->_current.getY() * BOARD_LEN + this->_current.getX() % BOARD_LEN;

		if (firstColum(possibleDestinationTile, offset) || eighthColum(possibleDestinationTile, offset)
			|| seventhColum(possibleDestinationTile, offset) || eighthColum(possibleDestinationTile, offset))
		{
			continue;
		}
		while (isValidTileCoordinate(possibleDestinationTile)) {
			int temp = possibleDestinationTile;
			possibleDestinationTile += offset;
			if (firstColum(temp, offset) || eighthColum(temp, offset))
			{
				continue;
			}
			if (isValidTileCoordinate(possibleDestinationTile))
			{
				if (board.getPieceInSpot(Spot(possibleDestinationTile % BOARD_LEN, possibleDestinationTile / BOARD_LEN))->isEmpty())
				{

					this->LegelMoves.push_back(Spot(possibleDestinationTile % BOARD_LEN, possibleDestinationTile / BOARD_LEN));
				}
				else
				{
					if (board.getPieceInSpot(Spot(possibleDestinationTile % BOARD_LEN, possibleDestinationTile / BOARD_LEN))->isWhite()
						!= this->isWhite())
					{
						this->LegelMoves.push_back(Spot(possibleDestinationTile % BOARD_LEN, possibleDestinationTile / BOARD_LEN));
					}
					break;
				}
			}
		}
	}

}