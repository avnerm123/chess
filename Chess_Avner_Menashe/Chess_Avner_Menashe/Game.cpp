#include "Game.h"


bool Game::move(string movement)
{
	vector<Spot> moves = this->getCoords(movement);
	
	this->_turn ? this->_turn = false : this->_turn = true;  //now im the Enemy
	King* myKing = FindTheEnemyKing();
	Piece* curr;
	this->_turn ? this->_turn = false : this->_turn = true;
	King* EnemyKing = FindTheEnemyKing();
	if (moves[0].getX() == moves[1].getX() && moves[1].getY() == moves[0].getY())
	{
		return false;
	}
	if (this->board.getPieceInSpot(moves[0])->isWhite() == this->board.getPieceInSpot(moves[1])->isWhite() && !this->board.getPieceInSpot(moves[1])->isEmpty())
	{
		return false;
	}
	if (this->board.getPieceInSpot(moves[0])->isWhite() != this->getTurn())
	{
		return false;
	}
	if (EnemyKing->isCheck(this->board) && board.getPieceInSpot(moves[0])->isValidMove(board, moves[1]))
	{
		curr = board.getPieceInSpot(moves[1]);
		this->board.move(moves[0], moves[1]);
		if (CheckForMate(curr->_current))
		{
			this->mate = true;
			this->_moves++;
			if (this->_moves > MOVES_FOR_PAT)
			{
				this->satelment = true;
			}
			this->_history.push_back(movement);
			return true;
		}
		this->_history.push_back(movement);
		return true;
	}
	if (myKing->isCheck(this->board) && board.getPieceInSpot(moves[0])->isValidMove(board, moves[1]))
	{
		curr = board.getPieceInSpot(moves[1]);
		this->board.move(moves[0], moves[1]);
		if (myKing->isCheck(this->board))
		{
			this->board.move(moves[1], moves[0]);
			this->board.setPieceInSpot(moves[1], curr);
			return false;
		}
		
		this->_history.push_back(movement);
		this->_moves++;
		if (this->_moves > MOVES_FOR_PAT)
		{
			this->satelment = true;
		}
		return true;
	}
	if(board.getPieceInSpot(moves[0])->isValidMove(board, moves[1]))
	{
	
		curr = board.getPieceInSpot(moves[1]);
		this->board.move(moves[0], moves[1]);
		if (myKing->isCheck(board))
		{
			this->board.move(moves[1], moves[0]);
			this->board.setPieceInSpot(moves[1], curr);
			return false;
		}
		if (CheckForMate(moves[1]))//if there was no check beforehand its a draw according to the rules of chess
		{
			this->satelment = true;
		}
		this->_history.push_back(movement);
		this->_moves++;
		if (this->_moves > MOVES_FOR_PAT)
		{
			this->satelment = true;
		}
		
		return true;
	}
	return false;
}


King* Game::FindTheEnemyKing()
{
	for (int i = 0; i < BOARD_LEN; i++)
	{
		for (int j = 0; j < BOARD_LEN; j++)
		{
			if (this->board.getPieceInSpot(Spot(j, i))->isWhite() != this->_turn
				and (this->board.getPieceInSpot(Spot(j, i))->getKind() == 'K'
				or this->board.getPieceInSpot(Spot(j, i))->getKind() == 'k'))
			{
				return (King*)this->board.getPieceInSpot(Spot(j, i));
			}
		}
	}
}

Board Game::getBoard()
{
	return this->board;
}


vector<Spot> Game::getCoords(string move)
{
	vector<Spot> moves;
	for (int i = 0; i < 4; i += 2)
	{
		int x = int(move[i]) - 97;
		int y = 7 - int(move[i+1]) + 49;
		moves.push_back(Spot(x, y));
	}
	return moves;
}


Game::Game()
{
	_moves = 0;
	_history.resize(MAX_PLAYS);
}

Game::Game(Board board)
{
	_moves = 0;
	_history.resize(MAX_PLAYS);
	this->board = board;
}


bool Game::CheckForMate(Spot Attacker)
{
	King* Enemyking = FindTheEnemyKing();

	this->board.getPieceInSpot(Attacker)->isValidMove(board, Enemyking->_current); //activate the getValidMoves Function!

	vector<Spot> legelMoves = this->board.getPieceInSpot(Attacker)->LegelMoves;
	legelMoves.push_back(Attacker);
	for (int k = 0; k < legelMoves.size(); k++)
	{
		for (int i = 0; i < BOARD_LEN; i++)
		{
			for (int j = 0; j < BOARD_LEN; j++)
			{
				if (this->board.getPieceInSpot(Spot(j, i))->isWhite() != this->_turn)
				{
					if (this->board.getPieceInSpot(Spot(j, i))->isValidMove(board, legelMoves[k]))
					{
						Board currBoard;
						for (int a = 0; a < BOARD_LEN; a++)
							for (int b = 0; b < BOARD_LEN; b++) currBoard.setPieceInSpot(Spot(b, a), board.getPieceInSpot(Spot(b, a)));
						currBoard.move(Spot(j, i), legelMoves[k]);
						King* currKing = (King*)currBoard.getPieceInSpot(Enemyking->_current);
						if (!currKing->isCheck(currBoard))
						{
							return false;
						}
						//return false;
					}
				}
			}
		}
	}
	if (!KingCanMoveOut(Attacker, Enemyking->_current))
	{
		return true;
	}


	return false;
}

void Game::printBoard()
{
	for (int i = 0; i < BOARD_LEN; i++)
	{
		for (int j = 0; j < BOARD_LEN; j++)
		{
			std::cout << this->board.getPieceInSpot(Spot(j, i))->getKind();
		}
		std::cout << std::endl;
	}
}


bool Game::getTurn()
{
	return this->_turn;
}


void Game::setTurn(bool turn)
{
	this->_turn = turn;
}


bool Game::KingCanMoveOut(Spot Attacker, Spot KingPos)
{
	this->board.getPieceInSpot(KingPos)->isValidMove(this->board, Attacker); //to activate the getValidMoves
	if(this->board.getPieceInSpot(KingPos)->isValidMove(this->board, Attacker)) //same
	{
		return true;
	}
	for (int i = 0; i < this->board.getPieceInSpot(KingPos)->LegelMoves.size(); i++)
	{
		Board currBoard;
		for (int a = 0; a < BOARD_LEN; a++)
			for (int b = 0; b < BOARD_LEN; b++) currBoard.setPieceInSpot(Spot(b, a), board.getPieceInSpot(Spot(b, a)));
		currBoard.move(KingPos, this->board.getPieceInSpot(KingPos)->LegelMoves[i]);
		King* currKing = (King*)currBoard.getPieceInSpot(this->board.getPieceInSpot(KingPos)->LegelMoves[i]);
		if (currKing->isCheck(currBoard))
		{
			return true;
		}
	}
	return false;
}