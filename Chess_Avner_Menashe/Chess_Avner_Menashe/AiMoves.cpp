#include "AiMoves.h"
int AiMoves::getValueKind(char kind)
{
	switch(kind)
	{
	case 'p':
		return BPAWN;
	case 'n':
		return BKNIGHT;
	case 'b':
		return BBISHOP;
	case 'r':
		return BROOK;
	case 'q':
		return BQUEEN;
	case 'k':
		return BKING;
	case 'P':
		return WPAWN;
	case 'N':
		return WKNIGHT;
	case 'B':
		return WBISHOP;
	case 'R':
		return WROOK;
	case 'Q':
		return WQUEEN;
	case 'K':
		return WKING;
	};
	return 0;
}

int AiMoves::getPlace(char kind)
{
	switch (kind)
	{
	case 'p':
		return PAWN_PLACE;
	case 'n':
		return KNIGHT_PLACE;
	case 'b':
		return BISHOP_PLACE;
	case 'r':
		return ROOK_PLACE;
	case 'q':
		return QUEEN_PLACE;
	case 'k':
		return KINGPLACE;
	case 'P':
		return PAWN_PLACE;
	case 'N':
		return KNIGHT_PLACE;
	case 'B':
		return BISHOP_PLACE;
	case 'R':
		return ROOK_PLACE;
	case 'Q':
		return QUEEN_PLACE;
	case 'K':
		return KINGPLACE;
	};
	return 0;
}

int AiMoves::getWorth(Board board)
{
	int counter = 0;
	for (int i = 0; i < BOARD_LEN; i++)
	{
		for(int j = 0; j < BOARD_LEN; j++)
		{
			counter += getValueKind(board.getPieceInSpot(Spot(j, i))->getKind());
			counter += this->locations_white[getPlace((board.getPieceInSpot(Spot(j, i))->getKind()))][j][i];
		}
	}
	return counter;
}

move AiMoves::findMin(BST* curr_node)
{
	if (!curr_node->hesLeft())
	{
		return curr_node->getData();
	}
	else
	{
		findMin(curr_node->getLeft());
	}
}

move AiMoves::findMax(BST * curr_node)
{
	if (!curr_node)
	{
	}
	else if (!curr_node->hesRight())
	{
		return curr_node->getData();
	}
	else
	{
		findMax(curr_node->getRight());
	}
}

AiMoves::AiMoves(Board board)
{
	setBT(board);

}

AiMoves::AiMoves()
{
}

std::string toChessNotes(sf::Vector2f p)
{
	std::string s = "";
	s += char(p.x  + 97);
	s += char(7 - p.y + 49);
	return s;
}

void AiMoves::setBT(Board board) // creates a tree of all the possiable moves, and their worth
{
	Game game(board);
	std::string movement = "";
	this->possibale_moves = new BST();
	for (int i = 0; i < BOARD_LEN; i++)
	{
		for (int j = 0; j < BOARD_LEN; j++)
		{
			for (int k = 0; k < BOARD_LEN; k++)
			{
				for (int l = 0; l < BOARD_LEN; l++)
				{
					if (!board.getPieceInSpot(Spot(j,i))->isEmpty())
					{
						if (board.getPieceInSpot(Spot(j,i))->isWhite() == board.getPieceInSpot(Spot(l,k))->isWhite())
						{
							continue;
						}
					}
					if (!board.getPieceInSpot(Spot(j, i))->isEmpty())
					{
						continue;
					}
					Game currGame(board);
					movement += toChessNotes(sf::Vector2f((float)i , (float)j ));
					movement += toChessNotes(sf::Vector2f((float)k , (float)l));
					if (currGame.move(movement))
					{
						this->possibale_moves->Insert(this->possibale_moves, move(movement, this->getWorth(currGame.getBoard())));
						
					}
					movement = "";
				}
			}
		}
	}
}

std::string AiMoves::getNextAiMove(Board board)
{
	BST* curr = nullptr;
	Board currBoard = board;
	/*
	for (int i = 0; i < 3; i++)
	{
		setBT(currBoard);
		Game currGame(currBoard);
		if (i % 2 == 0)
		{
			move nextMove = this->findMax(this->possibale_moves);
		}
		else
		{
			move nextMove = this->findMin(this->possibale_moves);
		}
		curr->Insert(curr, nextMove);
		currGame.move(nextMove.movement);
		currBoard = currGame.getBoard();
	}
	return this->findMax(curr).movement;*/
	setBT(currBoard);
	move nextMove = this->findMax(this->possibale_moves);
	return nextMove.movement;
}
