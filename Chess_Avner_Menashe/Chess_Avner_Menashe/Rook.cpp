#include "Rook.h"

bool Rook::isValidTileCoordinate(int currentCandidate)
{
	return currentCandidate >= 0 && currentCandidate < 64;
}


Rook::Rook(char kind, bool isWhite, bool isEmpty, int x, int y) : Piece(kind, isWhite, isEmpty, x, y)
{
}

bool Rook::firstColum(int tile, int offset) {
	int firstColumArr[] = { 0,8,16,24,32,40,43,56 };
	bool temp;

	for (int i : firstColumArr)
	{
		if (tile == i)
		{
			temp = true;
			break;
		}
		else {
			temp = false;
		}
	}

	return temp && (offset == -1);
}

bool Rook::eighthColum(int tile, int offset) {
	int eighthColumArr[] = { 7,15,23,31,39,47,55,63 };
	bool temp;

	for (int i : eighthColumArr)
	{
		if (tile == i)
		{
			temp = true;
			break;
		}
		else {
			temp = false;
		}
	}

	return temp && (offset == 1);
}


bool Rook::isValidMove(Board board, Spot next)
{
	if (!board.getPieceInSpot(next)->isEmpty())
	{
		if (this->isWhite() == board.getPieceInSpot(next)->isWhite())
		{
			return false;
		}
	}
	if (this->isEmpty())
	{
		return false;
	}

	this->getValidMoves(board);
	
	for (Spot curr : this->LegelMoves)
	{
		if (curr.getX() == next.getX() && curr.getY() == next.getY())
		{
			return true;
		}
	}
	return false;
}

void Rook::getValidMoves(Board board)
{
	this->LegelMoves.resize(0);
	int possibleDestinationTile;
	int offset;
	for (int i = 0; i < 4; i++)
	{
		offset = CANDIDATE_MOVE_COORDINATES[i];
		possibleDestinationTile = this->_current.getY() * BOARD_LEN + this->_current.getX() % BOARD_LEN;

		if (firstColum(possibleDestinationTile, offset) || eighthColum(possibleDestinationTile, offset))
		{
			continue;
		}
		while (isValidTileCoordinate(possibleDestinationTile)) {
			int temp = possibleDestinationTile;
			possibleDestinationTile += offset;
			if (firstColum(temp, offset) || eighthColum(temp, offset))
			{
				continue;
			}
			if (isValidTileCoordinate(possibleDestinationTile))
			{
				if (board.getPieceInSpot(Spot(possibleDestinationTile % BOARD_LEN, possibleDestinationTile / BOARD_LEN))->isEmpty())
				{

					this->LegelMoves.push_back(Spot(possibleDestinationTile % BOARD_LEN, possibleDestinationTile / BOARD_LEN));
				}
				else
				{
					if (board.getPieceInSpot(Spot(possibleDestinationTile % BOARD_LEN, possibleDestinationTile / BOARD_LEN))->isWhite()
						!= this->isWhite())
					{
						this->LegelMoves.push_back(Spot(possibleDestinationTile % BOARD_LEN, possibleDestinationTile / BOARD_LEN));
					}
					break;
				}
			}
		}
	}
}