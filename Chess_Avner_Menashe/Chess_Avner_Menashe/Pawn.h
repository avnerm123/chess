#pragma once
#include "Piece.h"

class Piece;

class Pawn :
	public Piece
{
	bool isValidTileCoordinate(int currentCandidate);
	bool eighthColum(int tile);
	bool firstColum(int tile);
	const int CANDIDATE_MOVE_COORDINATES[4] = { 7, 8, 9, 16 };
	bool moved;
public:

	Pawn(char kind, bool isWhite, bool isEmpty, int x, int y);
	void getValidMoves(Board board);
	bool isValidMove(Board, Spot next);
};


