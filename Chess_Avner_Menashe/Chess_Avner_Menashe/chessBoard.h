#pragma once
#define AMOUNT_OF_FIGURS 32
#include <SFML\Graphics.hpp>
#include "Game.h"
#include "graphicsFunctions.h"
#include "AiMoves.h"

class AiMoves;


class chessBoard
{
	int board[8][8] =
	{ -1,-2,-3,-4,-5,-3,-2,-1,
	 -6,-6,-6,-6,-6,-6,-6,-6,
	  0, 0, 0, 0, 0, 0, 0, 0,
	  0, 0, 0, 0, 0, 0, 0, 0,
	  0, 0, 0, 0, 0, 0, 0, 0,
	  0, 0, 0, 0, 0, 0, 0, 0,
	  6, 6, 6, 6, 6, 6, 6, 6,
	  1, 2, 3, 4, 5, 3, 2, 1 };

	sf::Sprite f[AMOUNT_OF_FIGURS];

public:
	void move(std::string str);
	void loadPosition(std::string position, sf::RectangleShape(&rectangle)[BOARD_LEN*BOARD_LEN]);

	chessBoard(bool Ai);
	~chessBoard();
};

