#include "Piece.h"

Piece::Piece(char kind, bool isWhite, bool _isEmpty, int x, int y)
{
	this->LegelMoves.reserve(20);
	this->LegelMoves.begin();
	this->_kind = kind;
	this->_isWhite = isWhite;
	this->_isEmpty = _isEmpty;
	this->_current = Spot(x, y);
}


bool Piece::isWhite()
{
	return this->_isWhite;
}

void Piece::setWhite(bool isWhite)
{
	this->_isWhite = isWhite;
}



char Piece::getKind()
{
	return this->_kind;
}



bool Piece::isEmpty()
{
	return this->_isEmpty;
}



void Piece::SetKind(char kind)
{
	this->_kind = kind;
}



void Piece::SetEmpty(bool isEmpty)
{
	this->_isEmpty = isEmpty;
}
