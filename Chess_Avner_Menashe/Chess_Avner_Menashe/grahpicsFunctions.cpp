#include <iostream>;
#include "graphicsFunctions.h";



std::string toChessNote(sf::Vector2f p)
{
	std::string s = "";
	s += char(p.x / size + 97);
	s += char(7 - p.y / size + 49);
	return s;
}



sf::Vector2f toCoord(char a, char b)
{
	int x = int(a) - 97;
	int y = 7 - int(b) + 49;
	return sf::Vector2f(x*size, y*size);
}



