#include "Pawn.h"
#include "Queen.h"
Pawn::Pawn(char kind, bool isWhite, bool isEmpty, int x, int y) : Piece(kind, isWhite, isEmpty, x, y)
{
	this->moved = false;
}

bool Pawn::isValidMove(Board board, Spot next)
{
	if (!board.getPieceInSpot(next)->isEmpty())
	{
		if (this->isWhite() == board.getPieceInSpot(next)->isWhite())
		{
			return false;
		}
	}
	if (this->isEmpty())
	{
		return false;
	}
	this->getValidMoves(board);
	for (int i = 0; i < this->LegelMoves.size(); i++)
	{
		Spot curr = this->LegelMoves[i];
		if (curr.getX() == next.getX() && curr.getY() == next.getY())
		{
			this->moved = true;
			if (this->_current.getY() == 1 && this->isWhite())
			{
				Queen* curr = new Queen('Q', this->isWhite(), this->isEmpty(), this->_current.getX(), this->_current.getY());
				board.setPieceInSpot(this->_current, curr);
			}
			if (this->_current.getY() == 7 && !this->isWhite())
			{
				Queen* curr = new Queen('q', this->isWhite(), this->isEmpty(), this->_current.getX(), this->_current.getY());
				board.setPieceInSpot(this->_current, curr);
			}
			
			return true;
		}
	}
	return false;
}


bool Pawn::isValidTileCoordinate(int currentCandidate) {
	return currentCandidate >= 0 && currentCandidate < 64;
}

bool Pawn::eighthColum(int tile) {
	int eighthColumArr[] = { 7,15,23,31,39,47,55,63 };
	bool temp = false;

	for (int i : eighthColumArr)
	{
		if (tile == i)
		{
			temp = true;
			break;
		}
		else
			temp = false;
	}

	return temp;
}

bool Pawn::firstColum(int tile) {
	int firstColumArr[] = { 0,8,16,24,32,40,43,56 };
	bool temp = false;

	for (int i : firstColumArr)
	{
		if (tile == i)
		{
			temp = true;
			break;
		}
		else
			temp = false;
	}

	return temp;
}




void Pawn::getValidMoves(Board board) {
	this->LegelMoves.resize(0);
	int possibleDestinationTile;
	int direction;
	int pieceTile = (this->_current.getY() * BOARD_LEN + this->_current.getX() % BOARD_LEN);
	this->isWhite() ? direction = -1 : direction = 1;
	for (int offset : this->CANDIDATE_MOVE_COORDINATES)
	{
		possibleDestinationTile = (this->_current.getY() * BOARD_LEN + this->_current.getX() % BOARD_LEN) + (direction * offset);

		if (!isValidTileCoordinate(possibleDestinationTile))
		{
			continue;
		}

		if (offset == 8 && (board.getPieceInSpot(Spot(possibleDestinationTile % BOARD_LEN, possibleDestinationTile / BOARD_LEN))->isEmpty()))
		{
			this->LegelMoves.push_back(Spot(possibleDestinationTile % BOARD_LEN, possibleDestinationTile / BOARD_LEN));
		}

		//TWO STEPS
		if (offset == 16 && !this->moved)
		{
			int behindCandidate = pieceTile + (direction * 8);
			if ((board.getPieceInSpot(Spot(behindCandidate % BOARD_LEN, behindCandidate / BOARD_LEN))->isEmpty())
				&& (board.getPieceInSpot(Spot(possibleDestinationTile % BOARD_LEN, possibleDestinationTile / BOARD_LEN))->isEmpty()))
			{
				this->LegelMoves.push_back(Spot(possibleDestinationTile % BOARD_LEN, possibleDestinationTile / BOARD_LEN));
			}
		}


		if (offset == 7 && !((eighthColum(pieceTile) && this->isWhite() || (firstColum(pieceTile) && !this->isWhite()))))
		{
			if ((!board.getPieceInSpot(Spot(possibleDestinationTile % BOARD_LEN, possibleDestinationTile / BOARD_LEN))->isEmpty()))
			{
				if (this->isWhite() != board.getPieceInSpot(Spot(possibleDestinationTile % BOARD_LEN, possibleDestinationTile / BOARD_LEN))->isWhite())
				{
					this->LegelMoves.push_back(Spot(possibleDestinationTile % BOARD_LEN, possibleDestinationTile / BOARD_LEN));
				}
			}
		}
		if (offset == 9 && !((firstColum(pieceTile) && this->isWhite()) || (eighthColum(pieceTile) && !this->isWhite())))
		{
			if (!board.getPieceInSpot(Spot(possibleDestinationTile % BOARD_LEN, possibleDestinationTile / BOARD_LEN))->isEmpty())
			{
				if (this->isWhite() != board.getPieceInSpot(Spot(possibleDestinationTile % BOARD_LEN, possibleDestinationTile / BOARD_LEN))->isWhite())
				{
					this->LegelMoves.push_back(Spot(possibleDestinationTile % BOARD_LEN, possibleDestinationTile / BOARD_LEN));
				}
			}
		}

	}

}

