#include "Spot.h"

Spot::Spot(int x, int y)
{
	this->setX(x);
	this->setY(y);
}

Spot::Spot()
{
	this->_x = 0;
	this->_y = 0;
}

int Spot::getX()
{
	return this->_x;
}

int Spot::getY()
{
	return this->_y;
}

void Spot::setX(int x)
{
	if (x < 0 or x > 7)
	{

	}
	else
	{
		this->_x = x;
	}
}

void Spot::setY(int y)
{
	if (y < 0 or y > 7)
	{
		
	}
	else
	{
		this->_y = y;
	}
}
