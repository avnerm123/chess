#pragma once
#include "Piece.h"

class Piece;

class King : public Piece
{
	const int CANDIDATE_MOVE_COORDINATES[8] = { -9, -8, -7, -1, 1, 8, 7, 9 };
	bool firstColum(int pieceTile, int offset);
	bool eighthColum(int pieceTile, int offset);
	bool isValidTileCoordinate(int currentCandidate);
public:
	King(char kind, bool isWhite, bool isEmpty, int x, int y);
	bool isValidMove(Board, Spot next);
	bool isCheck(Board board);
	void getValidMoves(Board board);

};
