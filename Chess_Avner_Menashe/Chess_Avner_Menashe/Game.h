#pragma once
#include "Board.h"
#include <string>
#include "King.h"
#include <iostream>
#define MOVES_FOR_PAT 50
#define WHITE true
#define BLACK false
#define NUM_CONV 48
#define A_ASCII 97
#define MAX_PLAYS 1000

using std::string;
using std::vector;

class Game
{
	int _moves;
	bool _turn = WHITE;
	vector < string> _history;
	Board board;
	bool KingCanMoveOut(Spot Attacker, Spot KingPos);
public:

	Game();
	Game(Board board);
	bool move(string);
	King* FindTheEnemyKing();
	Board getBoard();
	vector <Spot> getCoords(string);
	bool CheckForMate(Spot Attacker);
	void printBoard();
	bool getTurn();
	void setTurn(bool turn);
	bool mate = false;
	bool satelment = false;
};

