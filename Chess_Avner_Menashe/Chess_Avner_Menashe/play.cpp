#include "play.h"
#include "chessBoard.h"


play::play()
{
	sf::RenderWindow window(sf::VideoMode(w, h), "Chess The Game Of Kings!");

	texture[0].loadFromFile("images/play-button.png");
	texture[1].loadFromFile("images/refresh-button.png");
	texture[2].loadFromFile("images/board.png");

	rectangle.setSize(sf::Vector2f(w, h));
	sprite[0].setTexture(texture[0]);
	sprite[1].setTexture(texture[1]);
	sprite[2].setTexture(texture[2]);
	sprite[0].setScale(0.75f, 1.0f);
	sprite[1].setScale(0.15f, 0.20f);
	sprite[2].setScale(0.73f, 0.73f);
	sprite[0].setPosition(120, 20);
	sprite[1].setPosition(400, 20);
	sprite[2].setPosition(0, 0);
	rectangle.setFillColor(sf::Color::White);


	while (window.isOpen())
	{
		sf::Vector2i pos = sf::Mouse::getPosition(window);
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed) {
				window.close();
			}
			if (sf::Mouse::isButtonPressed(sf::Mouse::Right) || sf::Mouse::isButtonPressed(sf::Mouse::Left)) { // 1 v 1
				if (sprite[0].getGlobalBounds().contains(pos.x, pos.y)) {
					window.close();
					chessBoard graphics(false);
				}

				if (sprite[1].getGlobalBounds().contains(pos.x, pos.y)) // 1 v ai
				{
					window.close();
					chessBoard graphics(true);

				}
			}
		}

		window.clear();
		window.draw(rectangle);
		window.draw(sprite[2]);
		window.draw(sprite[0]);
		window.draw(sprite[1]);
		window.display();
	}
}



play::~play()
{
}
