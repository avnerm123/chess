#include "Piece.h"

class Piece;

class Bishop : public Piece {

	const int CANDIDATE_MOVE_COORDINATES[4] = { -9, -7, 9, 7 }; //to calc Mate
	bool firstColum(int pieceTile, int offset);
	bool eighthColum(int pieceTile, int offset);
	bool isValidTileCoordinate(int currentCandidate);
public:
	Bishop(char kind, bool isWhite, bool isEmpty, int x, int y);
	bool isValidMove(Board, Spot next);
	void getValidMoves(Board board);

};

