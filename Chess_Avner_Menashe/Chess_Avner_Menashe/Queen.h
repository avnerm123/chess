#pragma once
#include "Piece.h"
class Queen :
	public Piece
{


public:
	Queen(char kind, bool isWhite, bool isEmpty, int x, int y);
	bool isValidMove(Board, Spot next);
	void getValidMoves(Board board);
};

