#include "BST.h"


BST::BST(move value)
{
	data = value;
	left = right = nullptr;
}

BST::BST() : left(NULL), right(NULL), data()
{
}

bool BST::hesRight()
{
	return this->right != nullptr;
}

bool BST::hesLeft()
{
	return this->left != nullptr;
}

BST * BST::getRight()
{
	return this->right;
}

BST * BST::getLeft()
{
	return this->left;
}

move BST::getData()
{
	return this->data;
}

// Insert function definition. 
BST* BST::Insert(BST *root, move curr)
{
	if (!root)
	{
		// Insert the first node, if root is NULL. 
		return new BST(curr);
	}

	// Insert data. 
	if (curr.worth > root->data.worth)
	{
		// Insert right node data, if the 'value' 
		// to be inserted is greater than 'root' node data. 

		// Process right nodes. 
		root->right = Insert(root->right, curr);
	}
	else
	{
		// Insert left node data, if the 'value'  
		// to be inserted is greater than 'root' node data. 

		// Process left nodes. 
		root->left = Insert(root->left, curr);
	}

	// Return 'root' node, after insertion. 
	return root;
}


void BST::Inorder(BST *root) //for debuging
{
	if (!root)
	{
		return;
	}
	Inorder(root->left);
	std::cout << root->data.worth << std::endl;
	Inorder(root->right);
}

