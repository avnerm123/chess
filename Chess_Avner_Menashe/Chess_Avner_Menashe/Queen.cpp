#include "Queen.h"
#include "Bishop.h"
#include "Rook.h"
Queen::Queen(char kind, bool isWhite, bool isEmpty, int x, int y) :Piece(kind, isWhite, isEmpty, x, y)
{
}


bool Queen::isValidMove(Board board, Spot next)
{
	if (!board.getPieceInSpot(next)->isEmpty())
	{
		if (this->isWhite() == board.getPieceInSpot(next)->isWhite())
		{
			return false;
		}
	}
	if (this->isEmpty())
	{
		return false;
	}
	this->getValidMoves(board);
	for (Spot curr : this->LegelMoves)
	{
		if (curr.getX() == next.getX() && curr.getY() == next.getY())
		{
			return true;
		}
	}
	return false;
}



void Queen::getValidMoves(Board board)
{
	this->LegelMoves.resize(0);
	Bishop myB(this->getKind(), this->isWhite(), this->isEmpty(), this->_current.getX(), this->_current.getY());
	myB.getValidMoves(board);
	Rook myR(this->getKind(), this->isWhite(), this->isEmpty(), this->_current.getX(), this->_current.getY());
	myR.getValidMoves(board);
	for (int i = 0; i < myB.LegelMoves.size(); i++)
	{
		this->LegelMoves.push_back(myB.LegelMoves[i]);
	}
	for (int j = 0; j < myR.LegelMoves.size(); j++)
	{
		this->LegelMoves.push_back(myR.LegelMoves[j]);
	}
}