#include "King.h"


bool King::firstColum(int pieceTile, int offset) {
	int firstColumArr[] = { 0,8,16,24,32,40,43,56 };
	bool temp = false;

	for (int i : firstColumArr)
	{
		if (pieceTile == i)
		{
			temp = true;
			break;
		}
		else
			temp = false;

	}

	return temp && (offset == -9 || offset == -1 || offset == 7);
}

bool King::eighthColum(int pieceTile, int offset) {
	int eighthColumArr[] = { 7,15,23,31,39,47,55,63 };
	bool temp = false;

	for (int i : eighthColumArr)
	{
		if (pieceTile == i)
		{
			temp = true;
			break;
		}
		else
			temp = false;
	}

	return temp && (offset == -7 || offset == 1 || offset == 9);
}



bool King::isValidTileCoordinate(int currentCandidate)
{
	return currentCandidate >= 0 && currentCandidate < 64;
}

King::King(char kind, bool isWhite, bool isEmpty, int x, int y) :Piece(kind, isWhite, isEmpty, x, y)
{
}



bool King::isValidMove(Board board, Spot next)
{
	if (!board.getPieceInSpot(next)->isEmpty())
	{
		if (this->isWhite() == board.getPieceInSpot(next)->isWhite())
		{
			return false;
		}
	}
	if (this->isEmpty())
	{
		return false;
	}
	this->getValidMoves(board);
	for (int i = 0; i < this->LegelMoves.size(); i++)
	{
		Spot curr = this->LegelMoves[i];
		if (curr.getX() == next.getX() && curr.getY() == next.getY())
		{
			return true;
		}
	}
	return false;
}

bool King::isCheck(Board board)
{
	for (int i = 0; i < BOARD_LEN; i++)
	{
		for (int j = 0; j < BOARD_LEN; j++)
		{
			if (board.getPieceInSpot(Spot(j, i))->isValidMove(board, this->_current))
			{
				board.getPieceInSpot(Spot(j, i))->isValidMove(board, this->_current);
				return true;
			}
		}
	}
	return false;
}

void King::getValidMoves(Board board)
{
	this->LegelMoves.clear();
	int possibleDestinationTile;
	int offset;
	for (int i = 0; i < 8; i++)
	{
		offset = CANDIDATE_MOVE_COORDINATES[i];
		possibleDestinationTile = this->_current.getY() * BOARD_LEN + this->_current.getX() % BOARD_LEN;

		if (firstColum(possibleDestinationTile, offset) || eighthColum(possibleDestinationTile, offset))
		{
			continue;
		}
		while (isValidTileCoordinate(possibleDestinationTile)) {
			possibleDestinationTile += offset;
			if (firstColum(possibleDestinationTile, offset) || eighthColum(possibleDestinationTile, offset))
			{
				continue;
			}
			if (isValidTileCoordinate(possibleDestinationTile))
			{
				if (board.getPieceInSpot(Spot(possibleDestinationTile % BOARD_LEN, possibleDestinationTile / BOARD_LEN))->isEmpty())
				{

					this->LegelMoves.push_back(Spot(possibleDestinationTile % BOARD_LEN, possibleDestinationTile / BOARD_LEN));
				}
				else
				{
					if (board.getPieceInSpot(Spot(possibleDestinationTile % BOARD_LEN, possibleDestinationTile / BOARD_LEN))->isWhite()
						!= this->isWhite())
					{
						this->LegelMoves.push_back(Spot(possibleDestinationTile % BOARD_LEN, possibleDestinationTile / BOARD_LEN));
					}
					break;
				}
			}
		}
	}
}