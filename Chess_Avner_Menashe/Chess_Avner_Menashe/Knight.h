#pragma once
#include "Piece.h"

class Piece;

class Knight : public Piece 
{
	const int CANDIDATE_MOVE_COORDINATES[8] = { -17, -15, -10, -6, 6, 10, 15, 17 };

	bool firstColum(int pieceTile, int offset);
	bool secondColum(int pieceTile, int offset);
	bool seventhColum(int pieceTile, int offset);
	bool eighthColum(int pieceTile, int offset);
	bool isValidTileCoordinate(int currentCandidate);

public:
	Knight(char kind, bool isWhite, bool isEmpty, int x, int y);
	virtual ~Knight();
	bool isValidMove(Board, Spot next);
	
	void getValidMoves(Board board); // for Mate Checking// 
};

