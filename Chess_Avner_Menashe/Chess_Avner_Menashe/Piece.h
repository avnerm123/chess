#pragma once

#include "Spot.h"
#include <vector>
#include "Board.h"

using std::vector;

class Board;

class Piece
{
	bool _isWhite;
	char _kind;
	bool _isEmpty;

public:
	Spot _current;
	vector<Spot> LegelMoves;

	Piece(char kind, bool isWhite, bool _isEmpty, int x, int y);

	virtual bool isValidMove(Board board, Spot next) = 0;

	bool isWhite();
	void setWhite(bool isWhite);
	char getKind();
	bool isEmpty();
	void SetKind(char kind);
	void SetEmpty(bool isEmpty);

};

