#include "chessBoard.h"

void chessBoard::move(std::string str)
{
	sf::Vector2f oldPos = toCoord(str[0], str[1]);
	sf::Vector2f newPos = toCoord(str[2], str[3]);

	for (int i = 0; i < AMOUNT_OF_FIGURS; i++)
		if (f[i].getPosition() == newPos) f[i].setPosition(-100, -100); //setting them of the screen

	for (int i = 0; i < AMOUNT_OF_FIGURS; i++)
		if (f[i].getPosition() == oldPos) f[i].setPosition(newPos);
}



void chessBoard::loadPosition(std::string position, sf::RectangleShape (&rectangle)[BOARD_LEN*BOARD_LEN])
{
	int k = 0;
	for (int i = 0; i < BOARD_SIZE; i++)
	{
		for (int j = 0; j < BOARD_SIZE; j++)
		{
			int n = board[i][j];
			if (!n) continue;
			int x = abs(n) - 1;
			int y = n > 0 ? 1 : 0;
			f[k].setTextureRect(sf::IntRect(size*x, size*y, size, size));
			rectangle[k].setTextureRect(sf::IntRect(size*x, size*y, size, size));
			f[k].setPosition(size*j, size*i);
			rectangle[k].setPosition(size*j, size*i);
			
			k++;
		}
	}

	for (int i = 0; i < position.length(); i += 5)
	{
		move(position.substr(i, 4));
	}
}

chessBoard::chessBoard(bool Ai)
{
	std::string position = "";
	sf::Texture t1, t2;
	t1.loadFromFile("figures.png");
	t2.loadFromFile("board.png");

	sf::RectangleShape rectangle[BOARD_LEN*BOARD_LEN];
	bool wow = false;
	AiMoves ai;
	sf::Vector2f offset(28, 28);

	sf::RenderWindow window(sf::VideoMode(SIZE, SIZE), "Chess Program");

	for (int i = 0; i < AMOUNT_OF_FIGURS; i++) f[i].setTexture(t1);
	sf::Sprite sBoard(t2);

	loadPosition(position, rectangle);
	Game game;
	bool isMove = false;
	float dx = 0, dy = 0;
	sf::Vector2f oldPos, newPos;
	std::string str;
	int n = 0;
	bool isValidMove = false;
	int k = 0;
	

	for (int i = 0; i < 8; ++i)
	{
		for (int j = 0; j < 8; ++j) 
		{
			if ((i + j) % 2 == 0)
				rectangle[k].setFillColor(sf::Color::White);
			else
				rectangle[k].setFillColor(sf::Color::Blue);
			k++;
		}
	}



	while (window.isOpen())
	{
		sf::Vector2i pos = sf::Mouse::getPosition(window) - sf::Vector2i(offset);
		sf::Event e;
	
		while (window.pollEvent(e))
		{
			if (e.type == sf::Event::Closed)
			{
				window.close();
			}

			/////drag and drop///////
			if (e.type == sf::Event::MouseButtonPressed)
				if (e.key.code == sf::Mouse::Left)
					for (int i = 0; i < AMOUNT_OF_FIGURS; i++)
						if (f[i].getGlobalBounds().contains(pos.x, pos.y))
						{
							if (rectangle[i].getGlobalBounds().contains(pos.x, pos.y))
							{
								rectangle[i].setFillColor(sf::Color::Red);
							}
							isMove = true; n = i;
							dx = pos.x - f[i].getPosition().x;
							dy = pos.y - f[i].getPosition().y;
							oldPos = f[i].getPosition();
							
						}

			if (e.type == sf::Event::MouseButtonReleased)
				if (e.key.code == sf::Mouse::Left)
				{
					if (game.mate or game.satelment)
					{
						std::string winner;
						winner = !game.getTurn() ? winner = "white" : winner = "black";
						if (game.satelment && !game.mate)
						{
							std::cout << "It's a draw!";
						}
						else
						{
							std::cout << "Game Over " << winner << " won!";
						}
						system("PAUSE");
						window.close();
					}
					isMove = false;
					sf::Vector2f p = f[n].getPosition() + sf::Vector2f(size / 2, size / 2);
					newPos = sf::Vector2f(size*int(p.x / size), size*int(p.y / size));
					str = toChessNote(oldPos) + toChessNote(newPos);
					oldPos = toCoord(str[0], str[1]);
					newPos = toCoord(str[2], str[3]);
					int toX = newPos.x / size;
					int toY = newPos.y / size;
					int fromX = oldPos.x / size;
					int fromY = oldPos.y / size;
					if (game.getTurn() && Ai)
					{
						str = ai.getNextAiMove(game.getBoard());
					}
					if (game.move(str))
					{
						game.getTurn() ? game.setTurn(BLACK) : game.setTurn(WHITE);
						game.printBoard();
						move(str);
						if (oldPos != newPos) position += str + " ";
						f[n].setPosition(newPos);
						//str = getNextMove(position);


						board[toY][toX] = board[fromY][fromX];
						board[fromY][fromX] = 0;
						for (int i = 0; i < 32; i++) if (f[i].getPosition() == oldPos) n = i;

						/////animation///////
						for (int k = 0; k < 50; k++)
						{
							sf::Vector2f p = newPos - oldPos;
							f[n].move(p.x / 50, p.y / 50);
							window.draw(sBoard);
							for (int i = 0; i < 32; i++) f[i].move(offset);
							for (int i = 0; i < 32; i++) window.draw(f[i]); window.draw(f[n]);
							for (int i = 0; i < 32; i++) f[i].move(-offset);
							window.display();
						}

						move(str);  position += str + " ";
						f[n].setPosition(newPos);
					}
					else
					{
						int k = 0;
						for (int i = 0; i < BOARD_SIZE; i++)
						{
							for (int j = 0; j < BOARD_SIZE; j++)
							{
								int n = board[i][j];
								if (!n) continue;
								int x = abs(n) - 1;
								int y = n > 0 ? 1 : 0;
								f[k].setTextureRect(sf::IntRect(size*x, size*y, size, size));
								f[k].setPosition(size*j, size*i);
								k++;
							}
						}


					}
					isValidMove ? isValidMove = false : isValidMove = true;
				}

			if (isMove) f[n].setPosition(pos.x - dx, pos.y - dy);
			window.clear();
			window.draw(sBoard);
			for (int i = 0; i < 32; i++) f[i].move(offset);
			for (int i = 0; i < 32; i++) window.draw(f[i]); window.draw(f[n]);
			for (int i = 0; i < 32; i++) f[i].move(-offset);
			for (int i = 0; i < 64; i++) window.draw(rectangle[i]);
			window.display();


		}


	}

}


chessBoard::~chessBoard()
{
}
