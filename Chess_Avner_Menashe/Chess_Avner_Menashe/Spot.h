#pragma once
class Spot {
	int _x;
	int _y;
public:
	Spot(int x, int y);
	Spot();
	int getX();
	int getY();
	void setX(int x);
	void setY(int y);
};


