#pragma once
#include "Piece.h"
#include <vector>


using std::vector;
#define BOARD_LEN 8
class Piece;

class Board
{
	vector<vector <Piece*>> _soldiers;
	vector<Piece*> _whitePieces;
	vector<Piece*> _blackPieces;
public:
	Board();
	vector <vector<Piece*>> getBoard();
	void move(Spot fromSpot, Spot toSpot);
	Piece* getPieceInSpot(Spot curr);
	void setPieceInSpot(Spot curr, Piece* next);

};

