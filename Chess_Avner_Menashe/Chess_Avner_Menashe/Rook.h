#pragma once
#include "Piece.h"

class Piece;

class Rook : public Piece
{

	const int CANDIDATE_MOVE_COORDINATES[4] = { -8, -1, 1, 8 }; //to Calc Mate
	bool isValidTileCoordinate(int currentCandidate);
	bool firstColum(int tile, int offset);
	bool eighthColum(int tile, int offset);
public:
	Rook(char kind, bool isWhite, bool isEmpty, int x, int y);
	bool isValidMove(Board board, Spot next); 
	void getValidMoves(Board board);
};
