#pragma once

#include "Piece.h"


class Pawn : public Piece {
	bool moved = false;
	bool canEat(Board board, spot curr, spot next);
public:
	Pawn(char kind, bool isWhite);
	bool noPieceBetween(Board, spot curr, spot next);
	bool isValidMove(Board, spot curr, spot next);
};