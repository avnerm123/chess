#include "Piece.h"

Piece::Piece(char kind, bool isWhite)
{
	this->_kind = kind;
	this->_isWhite = isWhite;
	this->Error = 0;
}


char Piece::getKind()
{
	return this->_kind;
}

bool Piece::getWhite()
{
	return this->_isWhite;
}
