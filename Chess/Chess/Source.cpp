/*
This file servers as an example of how to use Pipe.h file.
It is recommended to use the following code in your project, 
in order to read and write information from and to the Backend
*/

#include "Pipe.h"
#include <iostream>
#include <thread>
#include "Game.h"
using std::cout;
using std::endl;
using std::string;


int main()
{
	int i;
	srand(time_t(NULL));
	Game game;
	Pipe p;
	bool isConnect = p.connect();
	bool turn = true;

	string ans;
	while (!isConnect)
	{
		cout << "cant connect to graphics" << endl;
		cout << "Do you try to connect again or exit? (0-try again, 1-exit)" << endl;
		std::cin >> ans;

		if (ans == "0")
		{
			cout << "trying connect again.." << endl;
			Sleep(5000);
			isConnect = p.connect();
		}
		else 
		{
			p.close();
			return 0;
		}
	}
	
	

	char msgToGraphics[1024];
	strcpy_s(msgToGraphics, game.getBoard());
	p.sendMessageToGraphics(msgToGraphics);   // send the board string

	// get message from graphics
	string msgFromGraphics = p.getMessageFromGraphics();

	while (msgFromGraphics != "quit")
	{
		// should handle the string the sent from graphics
		// according the protocol. Ex: e2e4           (move e2 to e4)
		
		// YOUR CODE
		strcpy_s(msgToGraphics, game.getBoard(msgFromGraphics, turn)); // msgToGraphics should contain the result of the operation

		/******* JUST FOR EREZ DEBUGGING ******/

	
		// return result to graphics		
		p.sendMessageToGraphics(msgToGraphics);   

		// get message from graphics
		
		msgFromGraphics = p.getMessageFromGraphics();
		if (turn)
		{
			std::cout << "true" << std::endl;
		}
		else
		{
			std::cout << "false" << std::endl;
		}

		if (msgToGraphics[0] == '1' or msgToGraphics[0] == '0')
		{
			if (!turn)
			{
				turn = true;
			}
			else
			{
				turn = false;
			}
		}
		
	}

	p.close();
	return 0;
}