#pragma once

#include "Piece.h"

class Knight : public Piece {

	int _x[8] = { 2, 1, -1, -2, -2, -1, 1, 2 };
	int _y[8] = { 1, 2, 2, 1, -1, -2, -2, -1 };

public:
	Knight(char kind, bool isWhite);
	virtual ~Knight();
	bool noPieceBetween(Board, spot curr, spot next);
	bool isValidMove(Board, spot curr, spot next);

};