#include "Pawn.h"



bool Pawn::canEat(Board board, spot curr, spot next)
{
	int fromX = curr.getX(), fromY = curr.getY(), toX = next.getX(), toY = next.getY();
	Piece* currP = board.getPieceInSpot(curr);
	Piece* nextP = board.getPieceInSpot(next);
	
	if (currP->getWhite() != nextP->getWhite() and nextP->getKind() != '#') //if you try to kill
	{
		if (!currP->getWhite())
		{
			if (fromY - 1 == toY && toX == fromX)
			{
				return false;
			}
		}
		else
		{
			if (fromY + 1 == toY && toX == fromX)
			{
				return false;
			}
		}
		return true;
	}

	return false;
}

Pawn::Pawn(char kind, bool isWhite) : Piece(kind, isWhite)
{
}

bool Pawn::noPieceBetween(Board board, spot curr, spot next)
{
	bool eat_possible = canEat(board, curr, next);
	Piece* currP = board.getPieceInSpot(curr);
	Piece* nextP = board.getPieceInSpot(next);
	int toX = next.getX(), toY = next.getY(), fromX = curr.getX(), fromY = curr.getY();
	if (currP->getKind() == '#')
	{
		return false;
	}
	if (eat_possible)
	{
		if (!currP->getWhite())
		{
			if (toX == fromX + 1 && toY == fromY + 1)
			{
				this->moved = true;
				return true;
			}
			if (toX == fromX - 1 && toY == fromY + 1)
			{
				this->moved = true;
				return true;
			}
			if (fromY + 1 == toY && fromX == toX)
			{
				this->moved = true;
				return true;
			}
		}
		else {
			if (toX == fromX - 1 && toY == fromY - 1)
			{
				this->moved = true;
				return true;
			}
			if (toX == fromX + 1 && toY == fromY - 1)
			{
				this->moved = true;
				return true;
			}
			if (fromY - 1 == toY && fromX == toX)
			{
				this->moved = true;
				return true;
			}
		}
	}
	else
	{
		if (nextP->getKind() != '#')
		{
			return false;
		}
		if (currP->getWhite())
		{
			if (fromY - 1 == toY && toX == fromX)
			{
				this->moved = true;
				return true;
			}
			if (!this->moved)
			{
				if (fromY + 1 > NUM_OF_LINES)
				{
					return false;
				}
				if (board.getPieceInSpot(spot(fromX , fromY + 1))->getKind() != '#')
				{
					if (fromY - 2 == toY && toX == fromX)
					{
						this->moved = true;
						return true;
					}
				}
			}
		}
		else
		{
			if (fromY + 1 == toY && toX == fromX)
			{
				this->moved = true;
				return true;
			}
			if (!this->moved)
			{
				if (fromY - 1 < 0)
				{
					return false;
				}
				if (board.getPieceInSpot(spot(fromX, fromY - 1))->getKind() != '#')
				{
					if (fromY + 2 == toY && toX == fromX)
					{
						this->moved = true;
						return true;
					}
				}
			}
		}
	}


	return false;
}

bool Pawn::isValidMove(Board board, spot curr, spot next)
{
	
	if (this->noPieceBetween(board, curr, next))
	{
		this->Error = 0;
		return true;
	}

	this->Error = 6;
	return false;
}


