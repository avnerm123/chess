#include "Spot.h"

spot::spot(int x, int y)
{
	this->_x = x;
	this->_y = y;
}

int spot::getX() const
{
	return _x;
}

int spot::getY() const
{
	return _y;
}

void spot::setX(int x)
{
	this->_x = x;
}

void spot::setY(int y)
{
	this->_y = y;
}