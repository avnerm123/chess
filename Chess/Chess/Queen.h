#include "Bishop.h"
#include "Rook.h"

class Queen : public Piece {
	
	bool isBishopMove(Board board, spot curr, spot next);
	bool isRookMove(Board board, spot curr, spot next);
public:
	Queen(char k, bool w);
	bool noPieceBetween(Board, spot curr, spot next);
	bool isValidMove(Board, spot curr, spot next);
};