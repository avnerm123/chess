#pragma once
#include "Piece.h"

class Rook : public Piece {

public:
	Rook(char kind, bool isWhite);
	virtual ~Rook();
	virtual bool isValidMove(Board board, spot curr, spot next); //for queen
	virtual bool noPieceBetween(Board board, spot curr, spot next); //for queen

};