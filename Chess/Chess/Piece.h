#pragma once
#include <iostream>
#include "Board.h"

#define MIN_CAP 64
#define MAX_CAP 91
#define MIN_UNCAP 96
#define MAX_UNCAP 123

class Board;

class Piece {
	bool _isWhite;
	char _kind;
public:
	int Error;
	Piece(char kind, bool isWhite);
	virtual bool noPieceBetween(Board , spot curr ,spot next) = 0;
	virtual bool isValidMove(Board , spot curr, spot next) = 0;
	char getKind();
	bool getWhite();
};