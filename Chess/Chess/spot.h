#pragma once
class spot{
	int _x;
	int _y;
public:
	spot(int x, int y);
	int getX() const;
	int getY() const;
	void setX(int x);
	void setY(int y);
};