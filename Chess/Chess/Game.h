#pragma once
#include "Board.h"
#include <string>

#define A_ASCII 97
#define NUM_CONV 48

class Game {
	Board board;
	spot findTheEnemyKing(bool turn);
public:
	Game();
	~Game();
	char* getBoard();
	char* getBoard(std::string move, bool turn);

};