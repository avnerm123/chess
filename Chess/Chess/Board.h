#pragma once
#include "spot.h"
#include "Piece.h"
#include <vector>

#define NUM_OF_LINES 8

class Piece;


class Board {
private:
	 std::vector<std::vector<Piece*>> spots;
public:
	Board();
	std::string getBoard();
	Piece* getPieceInSpot(spot curr);
	void setPieceInSpot(spot curr, Piece* next);
};