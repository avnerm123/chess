#include "Bishop.h"

int Bishop::Mm(int a, int b)
{
	if (a < b)return b - a;
	return a - b;
}

int Bishop::Mp(int a, int b)
{
	if (a > b)return b + a;
	return a + b;
}

Bishop::Bishop(char kind, bool isWhite) : Piece(kind, isWhite)
{
}

bool Bishop::noPieceBetween(Board board, spot curr, spot next)
{
	if (curr.getX() < next.getX() && curr.getY() < next.getY())
	{
		while (curr.getX() < next.getX() && curr.getY() < next.getY())
		{

			curr.setX(curr.getX() + 1);
			curr.setY(curr.getY() + 1);
			if (curr.getX() == next.getX() && curr.getY() == next.getY())
			{
				this->Error = 0;
				return true;
			}
			if (curr.getX() > NUM_OF_LINES or curr.getX() < -1 or curr.getY() < -1 or curr.getY() > NUM_OF_LINES)
			{
				this->Error = 6;
				return false;
			}
			if (board.getPieceInSpot(curr)->getKind() != '#')
			{
				this->Error = 6;
				return false;
			}
		}
		this->Error = 0;
		return true;
	}
	if (curr.getX() < next.getX() && curr.getY() > next.getY())
	{
		while (curr.getX() < next.getX() && curr.getY() > next.getY())
		{

			curr.setX(curr.getX() + 1);
			curr.setY(curr.getY() - 1);
			if (curr.getX() == next.getX() && curr.getY() == next.getY())
			{
				this->Error = 0;
				return true;
			}
			if (curr.getX() > NUM_OF_LINES or curr.getX() < -1 or curr.getY() < -1 or curr.getY() > NUM_OF_LINES)
			{
				this->Error = 6;
				return false;
			}
			if (board.getPieceInSpot(curr)->getKind() != '#')
			{
				this->Error = 6;
				return false;
			}
		}
		this->Error = 0;
		return true;
	}

	if (curr.getX() > next.getX() && curr.getY() < next.getY())
	{
		while (curr.getX() > next.getX() && curr.getY() < next.getY())
		{

			curr.setX(curr.getX() - 1);
			curr.setY(curr.getY() + 1);
			if (curr.getX() == next.getX() && curr.getY() == next.getY())
			{
				this->Error = 0;
				return true;
			}
			if (curr.getX() > NUM_OF_LINES or curr.getX() < -1 or curr.getY() < -1 or curr.getY() > NUM_OF_LINES)
			{
				this->Error = 6;
				return false;
			}
			if (board.getPieceInSpot(curr)->getKind() != '#')
			{
				this->Error = 6;
				return false;
			}
		}
		this->Error = 0;
		return true;
	}
	if (curr.getX() > next.getX() && curr.getY() > next.getY())
	{
		while (curr.getX() > next.getX() && curr.getY() < next.getY())
		{

			curr.setX(curr.getX() - 1);
			curr.setY(curr.getY() - 1);
			if (curr.getX() == next.getX() && curr.getY() == next.getY())
			{
				this->Error = 0;
				return true;
			}
			if (curr.getX() > NUM_OF_LINES or curr.getX() < -1 or curr.getY() < -1 or curr.getY() > NUM_OF_LINES)
			{
				this->Error = 6;
				return false;
			}
			if (board.getPieceInSpot(curr)->getKind() != '#')
			{
				this->Error = 6;
				return false;
			}
		}
		this->Error = 0;
		return true;
	}
	this->Error = 6;
	return false;

}

bool Bishop::isValidMove(Board board, spot curr, spot next)
{

	if (!this->noPieceBetween(board, curr, next))
	{
		return false;
	}

	if ((Mm(curr.getX(), next.getX()) == Mm(next.getY(), curr.getY())) || (Mp(next.getX(), curr.getX()) == Mp(next.getY(), curr.getY()))) //4 - 2 == 5 - 7
	{
		this->Error = 0;
		return true;
	}

	this->Error = 6;
	return false;
}
