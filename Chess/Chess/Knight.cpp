#include "Knight.h"

Knight::Knight(char kind, bool isWhite) : Piece(kind, isWhite)
{
}

Knight::~Knight()
{
	delete[] _x;
	delete[] _y;
}

bool Knight::noPieceBetween(Board, spot curr, spot next) //no need knight can skip!
{
	return true;
}

bool Knight::isValidMove(Board board, spot curr, spot next)
{

	for (int i = 0; i < NUM_OF_LINES; i++)
	{
		int toX2 = curr.getX() + this->_x[i];
		int toY2 = curr.getY() + this->_y[i];
		if (toX2 == next.getX() && toY2 == next.getY())
		{
			this->Error = 0;
			return true;
		}
	}
	this->Error = 6;
	return false;
}
