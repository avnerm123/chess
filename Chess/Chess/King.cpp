#include "King.h"

bool King::isCheck(Board board, spot king_coords)
{


	for (int i = 0; i < NUM_OF_LINES; i++)
	{
		for (int j = 0; j < NUM_OF_LINES; j++)
		{
			spot current(j, i);
			Piece* curr = board.getPieceInSpot(current);
			if (curr->isValidMove(board, current, king_coords))
			{
				curr->isValidMove(board, current, king_coords);
				this->_check = true;
				return true;
			}
		}
	}

	return false;

}

King::King(char k, bool w) : Piece(k, w)
{
	this->_check = false;
}

bool King::isValidMove(Board board, spot curr, spot next)
{
	int fromX = curr.getX(), fromY = curr.getY(), toX = next.getX(), toY = next.getY();

	if ((fromX - 1 == toX && fromY == toY) || (fromX == toX && fromY + 1 == toY) || (fromX == toX && fromY - 1 == toY) || (fromX + 1 == toX && fromY + 1 == toY) || (fromX - 1 == toX && fromY - 1 == toY) || (fromX - 1 == toX && fromY + 1 == toY) || (fromX + 1 == toX && fromY - 1 == toY))
	{
		this->Error = 0;
		return true;
	}
	this->Error = 6;
	return false;
}

bool King::noPieceBetween(Board board, spot curr, spot next)
{

	return true;
}
