#include "Game.h"
#include "Pawn.h"
#include "King.h"



Game::Game()
{
}

Game::~Game()
{
}

char* Game::getBoard()
{
	char msg[1024];
	std::string curr = board.getBoard();
	curr += "0";
	strcpy_s(msg, curr.c_str());
	return msg;
}

char* Game::getBoard(std::string move, bool turn)
{
	spot king_coords(0,0);
	Piece* currKing;
	Piece* currP;
	Piece* nextP;
	Piece* a;
	King* theKing = nullptr;
	bool was_check = false;
	int coords[4];
	char* msg = new char(2);

	msg[1] = NULL;

	coords[1] = 8 - ( (int)move[1] - NUM_CONV);
	coords[3] = 8 - ((int)move[3] - NUM_CONV);
	coords[0] =  (move[0] - A_ASCII);
	coords[2] =  (move[2] - A_ASCII);
	
	currP = this->board.getPieceInSpot(spot(coords[0], coords[1]));
	nextP = this->board.getPieceInSpot(spot(coords[2], coords[3]));
	
	if (currP->getWhite() != turn or currP->getKind() == '#')
	{
		msg[0] = '2';
		return msg;
	}
	if (currP->getWhite() == nextP->getWhite() && nextP->getKind() != '#')
	{
		msg[0] = '3';
		return msg;
	}

	for (int i = 0; i < NUM_OF_LINES; i++)
	{
		for (int j = 0; j < NUM_OF_LINES; j++)
		{
			currKing = this->board.getPieceInSpot(spot(i, j));
			if ((currKing->getKind() == 'K' and turn) or currKing->getKind() == 'k' and !turn)
			{
				theKing = (King*)currKing;
				king_coords.setX(i);
				king_coords.setY(j);
				was_check = theKing->isCheck(board, king_coords);
				i = NUM_OF_LINES;
				j = NUM_OF_LINES;

			}
		}
	}



	if (currP->isValidMove(board, spot(coords[0], coords[1]), spot(coords[2], coords[3])))
	{
		
		this->board.setPieceInSpot(spot(coords[2], coords[3]), currP);
		this->board.setPieceInSpot(spot(coords[0], coords[1]), new Pawn('#', false));
		if (was_check)
		{
			if (theKing->isCheck(this->board, king_coords))
			{
				this->board.setPieceInSpot(spot(coords[2], coords[3]), nextP);
				this->board.setPieceInSpot(spot(coords[0], coords[1]), currP);
				this->getBoard();
				msg[0] = '4';
				return msg;
			}
			else
			{
				this->getBoard();
				msg[0] = '0';
				return msg;
			}
		}
		else 
		{
			spot otherKing = findTheEnemyKing(turn);
			King* EnemyKing = (King*)this->board.getPieceInSpot(otherKing);
			if (EnemyKing->isCheck(this->board, otherKing)) 
			{
				msg[0] = '1';
				return msg;
			}
		}
		msg[0] = '0';
		return msg;
	}
	
	msg[0] = currP->Error + NUM_CONV;
	return msg;
}


spot Game::findTheEnemyKing(bool turn)
{
	Piece* currKing;

	for (int i = 0; i < NUM_OF_LINES; i++)
	{
		for (int j = 0; j < NUM_OF_LINES; j++)
		{
			currKing = this->board.getPieceInSpot(spot(i, j));
			if ((currKing->getKind() == 'K' and !turn) or currKing->getKind() == 'k' and turn)
			{
				return spot(i, j);
				i = NUM_OF_LINES;
				j = NUM_OF_LINES;

			}
		}
	}
}