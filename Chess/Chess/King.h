#pragma once
#include "Piece.h"

class King : public Piece {
private:
	bool _check;
public:
	bool isCheck(Board board, spot king_coords);
	King(char k, bool w);
	bool isValidMove(Board board, spot curr, spot next);
	bool noPieceBetween(Board board, spot curr, spot next);
};
