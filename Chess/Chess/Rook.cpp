#include "Rook.h"

Rook::Rook(char kind, bool isWhite) : Piece(kind, isWhite)
{
}

Rook::~Rook()
{
}

bool Rook::isValidMove(Board board, spot curr, spot next)
{
	int i;
	int fromRow = curr.getX(), fromCol = curr.getY(), toRow = next.getX(), toCol = next.getY();


	// Collision detection
	if (fromRow == toRow) { // Horizontal move

		int dx = (fromCol < toCol) ? 1 : -1;

		for (i = fromCol + dx; i != toCol; i += dx)
		{
			if (board.getPieceInSpot(spot(fromRow, i))->getKind() != '#')
			{
				this->Error = 6;
				return false;
			}
		}
		if (fromCol + dx == toCol)
		{
			this->Error = 6;
			return false;
		}
	}
	else if (fromCol == toCol) { // Vertical move
		int dy = (fromRow < toRow) ? 1 : -1;

		for (i = fromRow + dy; i != toRow; i += dy)
		{

			if (board.getPieceInSpot(spot(i, fromCol))->getKind() != '#')
			{
				this->Error = 6;
				return false;
			}
		}
		if (fromRow + dy == toRow)
		{
			this->Error = 6;
			return false;
		}
	}
	else { 
		// Not a valid rook move
		this->Error = 6;
		return false;
	}
	this->Error = 0;
	return true;
}


bool Rook::noPieceBetween(Board board,spot curr, spot next)
{
	return true;
}
