#include "Piece.h"

class Bishop : public Piece{
	int Mm(int a, int b);
	int Mp(int a, int b);
public:
	Bishop(char kind, bool isWhite);
	virtual bool noPieceBetween(Board, spot curr, spot next); //for queen 
	virtual bool isValidMove(Board, spot curr, spot next); //for queen
};