#include "Queen.h"


int m(int a, int b) {
	if (a < b)return b - a;
	return a - b;
}

bool Queen::isBishopMove(Board board, spot curr, spot next)
{
	return (m(next.getX(), curr.getX()) == m(next.getY(), curr.getY()));
}

bool Queen::isRookMove(Board board, spot curr, spot next)
{
	return curr.getX() == next.getX() || next.getY() == curr.getY();
}

Queen::Queen(char k, bool w) : Piece(k, w)
{
}

bool Queen::noPieceBetween(Board board, spot curr, spot next)
{
	if (isBishopMove(board, curr, next))
	{
		Bishop a(this->getKind(), this->getWhite());
		if ( a.isValidMove(board, curr, next))
		{
			this->Error = 0;
			return true;
		}

		this->Error = 6;
		return false;
	}

	if (isRookMove(board, curr, next))
	{
		Rook a(this->getKind(), this->getWhite());
		if (a.isValidMove(board, curr, next))
		{
			this->Error = 0;
			return true;
		}

		this->Error = 6;
		return false;
	}

	return false;
}

bool Queen::isValidMove(Board board, spot curr, spot next)
{
	return this->noPieceBetween(board, curr, next);
}
