#include "Board.h"
#include "Spot.h"
#include "Pawn.h"
#include "Knight.h"
#include "Queen.h"
#include "Rook.h"
#include "King.h"
#include <ostream>
Board::Board()
{
	this->spots.resize(NUM_OF_LINES);
	for (int i = 0; i < this->spots.size(); i++)
	{
		this->spots[i].resize(NUM_OF_LINES);
	}

	for (int i = 0; i < NUM_OF_LINES; i++) {
		this->spots[6][i] = new Pawn('P', true);
		this->spots[1][i] = new Pawn('p', false);
	}

	this->spots[0][0] = new Rook('r', false);
	this->spots[0][7] = new Rook('r', false);
	this->spots[0][2] = new Bishop('b', false);
	this->spots[0][5] = new Bishop('b', false);
	this->spots[0][1] = new Knight('n', false);
	this->spots[0][6] = new Knight('n', false);
	this->spots[0][3] = new Queen('q', false);
	this->spots[0][4] = new King('k', false);

	this->spots[7][0] = new Rook('R', true);
	this->spots[7][7] = new Rook('R', true);
	this->spots[7][2] = new Bishop('B', true);
	this->spots[7][5] = new Bishop('B', true);
	this->spots[7][1] = new Knight('N', true);
	this->spots[7][6] = new Knight('N', true);
	this->spots[7][3] = new Queen('Q', true);
	this->spots[7][4] = new King('K', true);

	for (int i = 2; i < 6; i++)
	{
		for (int j = 0; j < NUM_OF_LINES; j++)
		{
			this->spots[i][j] = new Pawn('#', false);


		}
	}

}

std::string Board::getBoard()
{

	std::string board = "";
	for (int i = 0; i < NUM_OF_LINES; i++)
	{
		for (int j = 0; j < NUM_OF_LINES; j++)
		{
			board += this->spots[i][j]->getKind();
			std::cout << this->spots[i][j]->getKind() << " ";
		}
		std::cout << std::endl;
	}
	return board;
}

Piece* Board::getPieceInSpot(spot curr)
{
	if (curr.getX() > NUM_OF_LINES or curr.getX() < 0 or curr.getY() > NUM_OF_LINES or curr.getY() < 0)
	{
		return new Pawn('#', false);
	}
	return this->spots[curr.getY()][ curr.getX()];
}

void Board::setPieceInSpot(spot curr, Piece* next)
{
	this->spots[curr.getY()][curr.getX()] = next;
}

